import {
  Association, DataTypes, HasManyAddAssociationMixin, HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin, HasManyGetAssociationsMixin, HasManyHasAssociationMixin, Model,
  ModelDefined, Optional, Sequelize
} from "sequelize";

const sequelize = new Sequelize(process.env.DB_SCHEMA || 'postgres',
  process.env.DB_USER || 'postgres',
  process.env.DB_PASSWORD || '',
  {
    host: process.env.DB_HOST || 'localhost',
    port: Number(process.env.DB_PORT || 5432),
    dialect: 'postgres',
    dialectOptions: {
      ssl: process.env.DB_SSL == "true"
    }
  });

// Atributos del model
interface DataAttributes {
  id: number;
  objectId: string;
  name: string;
  color: string | null;
  price: number | null;
}

interface DataInput extends Optional<DataAttributes, "id"> { }
interface DataOutput extends Required<DataAttributes> { }

class Data extends Model<DataAttributes, DataInput> implements DataAttributes {

  declare id: number;
  declare objectId: string;
  declare name: string;
  declare color: string | null;
  declare price: number | null;

  // timestamps!
  declare readonly createdAt: Date;
  declare readonly updatedAt: Date;

}

Data.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    objectId: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    name: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    color: {
      type: new DataTypes.STRING(128),
      allowNull: true,
    },
    price: {
      type: new DataTypes.DECIMAL(2),
      allowNull: true
    }
  },
  {
    tableName: "data",
    sequelize,
  }
);
export default Data
export { sequelize, DataInput, DataOutput }