import App from './app'

import * as bodyParser from 'body-parser'
import defaultMiddleware from './middleware/default'

import IndexController from './controllers/index.controller'
// import HomeController from './controllers/home/home.controller'

const port: number = Number(process.env.PORT || "5000");

const app = new App({
  port,
  controllers: [
    new IndexController(),
    //new PostsController()
  ],
  middleWares: [
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    defaultMiddleware
  ]
})

app.listen()
