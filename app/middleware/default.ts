import { Request, Response, NextFunction } from 'express'

const Middleware = (req: Request, resp: Response, next: NextFunction) => {

  console.log('Request:', req.method, req.path)
  next()
}

export default Middleware
