import * as express from 'express'
import { Request, Response } from 'express'
import controllerInterface from './base.controller'
import * as fs from 'fs'
import Data, { DataInput, DataOutput} from '../models/data'
import { Op }  from 'sequelize'
// @ts-ignore
import * as StreamArray  from 'stream-json/streamers/StreamArray'


export default class implements controllerInterface {

  public path = '/'
  public router = express.Router()

  constructor() {
    this.router.get(this.path, this.index)
    this.router.get('/start', this.start)
    this.router.get('/:id', this.get)
  }


  index(req: Request, res: Response) {
    res.render('index', { title: "Ejemplo de aplicación node-typescript" })
  }

  start(req: Request, res: Response) {
    let counter = 0;
    const readStream = fs.createReadStream('./data.json', { highWaterMark: 16 })
    const jsonStream = StreamArray.withParser()
    const db = require('../models/data')

    readStream.pipe(jsonStream);

    jsonStream.on('data', ({ key, value }: { key: string, value: any }) => {

      Data.create({
        objectId: value.id,
        name: value.name,
        color: value.color || "default color",
        price: value.price || 0
      })
        .catch((err: any) => {
          readStream.destroy();
          res.render('index', {
            title: 'Algo ha salido mal',
            result: counter,
            error: JSON.stringify(err)
          })
        });
      counter++;
    });

    jsonStream.on('end', () => {
      console.log('Trabajo terminado...');
      res.render('index', {
        title: 'Resultados del ejemplo',
        result: counter
      });
    });

  }

  get(req: Request, res: Response) {
    //return res.status(200).send('hola');
    Data.findAll({
      where: {
        [Op.or]: [
          {id: Number(req.params.id) || 0},
          {objectId: req.params.id}

        ]
      }
    })
      .then((Data) => {
        res.status(200).send(JSON.stringify(Data));
      })
      .catch((err: any) => {
        res.status(500).render('index', {
          title: 'Algo ha salido mal',
          result: 0,
          error: JSON.stringify(err)
        })
      });
  }
}